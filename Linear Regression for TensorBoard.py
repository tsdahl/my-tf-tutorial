
# coding: utf-8

# # Linear Regression for TensorBoard
# Fitting a Linear Model to synthetic house price data ($y = 2x$) with output for TensorBoard <br>
# y = W.x + b <br>
# Where: x is the house size (m<sup>2</sup>) and y is the predicted house price (£) <br>
# This is a slight modification of Soon Hin Khor's tutorial [Gentlest Introduction to Tensorflow #2](https://medium.com/all-of-us-are-belong-to-machines/gentlest-introduction-to-tensorflow-part-2-ed2a0a7a624f)

# In[2]:


import sys
import numpy as np
import tensorflow as tf

print('Python %d.%d.%d' % (sys.version_info[0], sys.version_info[1], sys.version_info[2]))
print('TensorFlow', tf.__version__)


# ## Defining the model

# In[3]:


x = tf.placeholder(tf.float32, [None, 1], name="x")

val_init_max = 0.01
W = tf.Variable(val_init_max * tf.random_uniform([1, 1]), name="W")
b = tf.Variable(val_init_max * tf.random_uniform([1]), name="b")

with tf.name_scope("y") as scope:
    y = tf.matmul(x, W) + b

y_ = tf.placeholder(tf.float32, [None, 1], name="y_")

# Collecting data in histogram for TensorBoard
W_hist = tf.summary.histogram("weights", W)
b_hist = tf.summary.histogram("biases", b)
y_hist = tf.summary.histogram("y", y)


# ## Define training
# Cost function $C=\frac{1}{n}\sum{(\overline{y}-y)^2}$.

# In[8]:


# The cost function, is the mean square error between output and target, mean((y_-y)**2)
with tf.name_scope("cost") as scope:
    cost = tf.reduce_mean(tf.square(y_ - y))
    # Summarize the costs for TensorBoard
    cost_sum = tf.summary.scalar("cost", cost)

    # Training using Gradient Descent to minimize cost
    learning_rate_init = 0.001
with tf.name_scope("learn_rate") as scope:
    learn_rate = tf.placeholder(tf.float32, shape=[])

with tf.name_scope("train") as scope:
    train_step = tf.train.GradientDescentOptimizer(learning_rate=learning_rate_init).minimize(cost)

# Create training set
training_set_size = 1000
actual_W = 2
actual_b = 10

train_xs = []
train_ys = []
for i in range(training_set_size):
    x_tmp = i % 10
    train_xs.append(x_tmp)
    train_ys.append(actual_W * (x_tmp) + actual_b)

train_xs = np.transpose([train_xs])
train_ys = np.transpose([train_ys])


# ## Running it

# In[10]:


sess = tf.Session()

# Merge all the summaries and write them out to /tmp/linreg_logs
merged = tf.summary.merge_all()
log_file = "/tmp/linreg"
writer = tf.summary.FileWriter(log_file, sess.graph)

init = tf.global_variables_initializer()
sess.run(init)

steps = 10000
batch_size = 1
for i in range(steps):
    if training_set_size == batch_size:
        batch_start_idx = 0
    else:
        batch_start_idx = (i * batch_size) % (training_set_size - batch_size)
    batch_end_idx = batch_start_idx + batch_size
    batch_xs = train_xs[batch_start_idx: batch_end_idx]
    batch_ys = train_ys[batch_start_idx: batch_end_idx]
    xs = np.array(batch_xs)
    ys = np.array(batch_ys)

    # Record summary data, and the accuracy every  steps
    record_period = 10
    if i % record_period == 0:
        train_feed = {x: train_xs, y_: train_ys}
        result = sess.run(merged, feed_dict=train_feed)
        writer.add_summary(result, i)
    else:
        feed = {x: xs, y_: ys, learn_rate: learning_rate_init / i}
        sess.run(train_step, feed_dict=feed)
#   print("y: %s" % sess.run(y, feed_dict=feed))
#     print("y_: %s" % ys)
    print("cost: %f" % sess.run(cost, feed_dict=feed))
    print("After %d iteration:" % i)
    print("W: %f" % sess.run(W))
    print("b: %f" % sess.run(b))

# NOTE: W should be close to actual_W, and b should be close to actual_b

