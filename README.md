# My TensorFlow Tutorial

Doing some tutorials to try out TensorFlow.

## Basics
* [Linear regression](https://medium.com/all-of-us-are-belong-to-machines/the-gentlest-introduction-to-tensorflow-248dc871a224)
* [Linear Regression with TensorBoard](https://medium.com/all-of-us-are-belong-to-machines/gentlest-introduction-to-tensorflow-part-2-ed2a0a7a624f) 
* [CNN](https://www.datacamp.com/community/tutorials/cnn-tensorflow-python)
* [RNN](https://medium.com/@erikhallstrm/hello-world-rnn-83cd7105b767)
* [LSTM](https://www.tensorflow.org/tutorials/sequences/recurrent)
* [GAN](https://wiseodd.github.io/techblog/2016/09/17/gan-tensorflow/)
* [Neuroevolution](https://eng.uber.com/deep-neuroevolution/)

## Reinforcement Learning
### Value Function Estimation
* [DQN](https://github.com/awjuliani/DeepRL-Agents/blob/master/Double-Dueling-DQN.ipynb)
* [DRQN](https://github.com/awjuliani/DeepRL-Agents/blob/master/Deep-Recurrent-Q-Network.ipynb)

### Policy Gradient Methods
#### Deterministic
* [Distributed Distributional DDPG, D4PG](https://github.com/msinto93/D4PG)

#### Stochastic
* [PPO](https://blog.openai.com/openai-baselines-ppo/)
* [A3C](https://github.com/awjuliani/DeepRL-Agents/blob/master/A3C-Doom.ipynb) ([T2](http://inoryy.com/post/tensorflow2-deep-reinforcement-learning/))

### Both
* [Smoothie](https://arxiv.org/pdf/1803.02348.pdf)

## Temporal Networks
* [Gated Recurrent Units](https://www.data-blogger.com/2017/08/27/gru-implementation-tensorflow/)
* [Gated Orthagonal Recurrent Units](https://github.com/jingli9111/GORU-tensorflow)
* [Attention](https://colab.research.google.com/github/tensorflow/tensorflow/blob/master/tensorflow/contrib/eager/python/examples/nmt_with_attention/nmt_with_attention.ipynb)
* [Transformer Networks](https://colab.research.google.com/github/tensorflow/tensor2tensor/blob/master/tensor2tensor/notebooks/hello_t2t.ipynb)
* [Fast Weights](https://github.com/GokuMohandas/fast-weights#)
* [TCN](https://medium.com/the-artificial-impostor/notes-understanding-tensorflow-part-3-7f6633fcc7c7)
* [Transformer-XL](https://ai.googleblog.com/2019/01/transformer-xl-unleashing-potential-of.html)

## Other RL Topics
* Value Function Representation
  * [Advantage Learning](https://medium.freecodecamp.org/improvements-in-deep-q-learning-dueling-double-dqn-prioritized-experience-replay-and-fixed-58b130cc5682)
  * [Smoothed Value Functions](https://arxiv.org/pdf/1803.02348.pdf)
* Models:
  * [World Models](https://worldmodels.github.io/)
  * [Neural Scene Representation](https://deepmind.com/blog/neural-scene-representation-and-rendering/)
* Variability Models
  * [Probabilistic Dynamics Models](http://papers.nips.cc/paper/7725-deep-reinforcement-learning-in-a-handful-of-trials-using-probabilistic-dynamics-models.pdf)
  * [Stocastic Ensamble Value Expansions](http://papers.nips.cc/paper/8044-sample-efficient-reinforcement-learning-with-stochastic-ensemble-value-expansion.pdf)
* Exploration:
  * [Intrinsic Motivation](https://pathak22.github.io/noreward-rl/)
  * [Information-Directed Exploration](https://www.mendeley.com/viewer/?fileId=3085e3c6-4c0a-bf21-6ba6-6d7abcf2f870&documentId=21a4a099-757c-3290-8204-49b3efa25c6e)
  * [Unsupervised Learning of Goal Spaces](https://github.com/flowersteam/Unsupervised_Goal_Space_Learning)
  * [Count-Based Exploration](https://arxiv.org/abs/1611.04717)
* State Representation:
  * [Robotic Priors](http://www.robotics.tu-berlin.de/fileadmin/fg170/Publikationen_pdf/Jonschkowski-15-AURO.pdf)
* Dynamic Updates
  * [Adaptive Step Size](https://www.aaai.org/ocs/index.php/AAAI/AAAI12/paper/view/5092/5494)

## Other Classification Topics
* [Oversampling and Thresholding for Imbalanced Data](https://arxiv.org/pdf/1710.05381.pdf)
* [DropBlock for Regularistion of Convolutional Layers](http://papers.nips.cc/paper/8271-dropblock-a-regularization-method-for-convolutional-networks)
* [Double Attention](http://papers.nips.cc/paper/7318-a2-nets-double-attention-networks)
* [Loss-Based Stepsize Adaptation](http://papers.nips.cc/paper/7318-a2-nets-double-attention-networks)

## Recognition and Segmentation
* [Siamese Neural Networks](https://www.cs.cmu.edu/~rsalakhu/papers/oneshot1.pdf)
* [Mask R-CNN](https://github.com/matterport/Mask_RCNN)
* [SiamMask](http://www.robots.ox.ac.uk/~qwang/SiamMask/)

## Benchmark Frameworks
* [Gym](https://gym.openai.com/)
* [PyBullet](https://github.com/bulletphysics/bullet3)
* [MuJoCo](http://www.mujoco.org/)
* [RoboSchool](https://github.com/openai/roboschool)
* [Dopamine](https://github.com/google/dopamine)
* [Fast.ai](https://docs.fast.ai/)
* [Coach](https://github.com/NervanaSystems/coach)
* [StarCraft](https://deepmind.com/blog/deepmind-and-blizzard-release-starcraft-ii-ai-research-environment/)
* [DeepMind Lab](https://github.com/deepmind/lab)